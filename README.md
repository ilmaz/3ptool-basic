# README #

Basic post-processing tools.

### What is this repository for? ###

- With the 3ptool-basic you can post-process the output results produced by any simulation made with SPEED.
- Plot selected time-histories
- Make PG maps
- Make movies to be reproduced by Paraview

### How do I get set up? ###

- Matlab software is the only pre-requisite

### Who do I talk to? ###

- For any queries contact speed.polimi@gmail.com
