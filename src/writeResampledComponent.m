%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%************************************************************************** 

function writeResampledComponent(path,rectxt,signal,dirSeparator,...
                                 t,vectorx,vectory,vectorz,...
                                 vectorp,vectoro,cost)


%------------------------------------------------------------------
num = size(vectorp,2);
if signal(1) == 'd'
    nomefilew=[char(path),'monitor4movie',dirSeparator,'DIS',dirSeparator,'',char(rectxt),'.',char(signal(1))];
elseif signal(1) == 'v'
    nomefilew=[char(path),'monitor4movie',dirSeparator,'VEL',dirSeparator,'',char(rectxt),'.',char(signal(1))];
elseif signal(1) == 'a'
    nomefilew=[char(path),'monitor4movie',dirSeparator,'ACC',dirSeparator,'',char(rectxt),'.',char(signal(1))];
end
    
fid = fopen(nomefilew,'w');
fprintf(fid,'%+9.5f\t %+9.5f\t %+9.5f\t %+9.5f\t %+9.5f\t %+9.5f\n',...
            [t; [0 vectorx(2:num).*cost]; [0 vectory(2:num).*cost]; ...
            [0 vectorz(2:num).*cost]; [0 vectorp(2:num).*cost];...
            [0 vectoro(2:num).*cost]]);
fclose(fid);
%------------------------------------------------------------------
        



