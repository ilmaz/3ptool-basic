

%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano
%         P.zza Leonardo da Vinci, 32
%         20133 Milano
%         Italy
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function [] = postprocessing(data)
% postprocessing: creation of the output
% OUTPUT:
%   monitor.d = folder containing displacement th
%   monitor4movie = folder containing:
%                   DIS = disp. th filtered
%                   VEL = vel. th filtered
%                   ACC = acc. th filtered
%                   SA  = spectra acc.
%                   SD  = spectra dis.
%   PGMAP = folder containing pgmaps for ArcGIS
%   MOVIE = folder containing frames for creating a movie with PARAVIEW
%

addpath src

clc;

disp('Starting output files generation...')


% Matlab parser to post-process results of a suite of eqk scenarios
% obtained from analyses by SPEED.

tstart=cputime;

% ***********************************************************
% ***                PARAMETER DECLARATION                ***
% ***********************************************************

%% SELECT THE OPERATING SISTEM
%UnixOrDos = 'Unix';             %ADMISSIBILE KEYWORDS: 'Unix', 'Dos '
UnixOrDos = data.OS;
if(isempty(data.OS) )
    disp('Plese Select the Operating System');
end

if UnixOrDos == 'Unix'
    dirSeparator = '/';
    path1 = [cd];
    disp('on Unix Filesystem')
    disp(' ')
elseif UnixOrDos == 'Dos '
    dirSeparator = '\';
    path1 = [pwd];
    disp('on Windows Filesystem')
    disp(' ')
end

%% DEFINE PARALLEL THREADS ON MATLAB
nlab = 1;   %numero processi paralleli matlab

%% MONITOR CONVERSION (from MONITORXXXX.D to monitorXXXXX.d)
iconv_mon = data.ICONV_MON; %converts MONITOR to monitor

%% SELECT MONITORS (0 selects all points defined in LS.input)
numMon = data.NUM_OF_MON;  % 0 prende tutti i monitor nel file LS

%% SCALE FACTOR  (1 = DISP in m, VEL in m/s, ACC in m/s^2)
scale = data.SCALE; % 1 = output in m m/s m/s/s

%% FILTERS PARAMETERS (used if filtra = 1)

filtra = data.FILTER; %1; % dummy variable: 1 = filter numerical results
n = 3;
freq_low = data.LP_FREQ; %0.05;
freq_high = data.HP_FREQ; %25.0;
freq_highz = freq_high;


%% COMPUTATION OF PSEUDO-DISP/PSEUDO-VEL/PSEUDO-ACC RESPONSE SÈECTRA
ispec = data.RES_SPEC;

%% TIME STEP FOR RESAMPLED SYNTHETICS
dt_resamp = data.RESAMPLED_DT; %0.05;

%% MOVIE TYPE
if(data.DIS == 1 && data.VEL == 0 && data.ACC == 0)
    imov = 'dis';
elseif(data.DIS == 0 && data.VEL == 1 && data.ACC == 0)
    imov = 'vel';
elseif(data.DIS == 0 && data.VEL == 0 && data.ACC == 1)
    imov = 'acc';
else
    imov = 'nan';
    disp('Multiple selection for movie. Please select only one button between DISP, VEL and ACC!')
end


%% RESPONSE SPECTRA PARAMETERS
% THESE PARAMETER ARE FIXED, DO NOT MODIFY THEM
Tmin = 0;
Tmax = 10;
dT = 0.05;
zeta = 0.05; % Damping
Tn = [Tmin:dT:Tmax];
nT = length(Tn);
toll = 1500;
nout_mrq = 7;



%% START OUTPUT FILE GENERATION

path = data.DIR;

path_mon1 = [path,'MONITORS',dirSeparator];
mkdir([path,'monitor.d',dirSeparator]);
path_monitor = [path,'monitor.d',dirSeparator];


% Read monitor coordinates from "LS.input"
nomefile_mon = [char(path),'LS.input'];
fid = fopen(nomefile_mon,'r');
tline = fgetl(fid);
num_mon = str2num(tline);
if numMon ~= 0
    num_mon = numMon;
end
fclose(fid);



mon_coor = textread(nomefile_mon,'','headerlines',1);
x_utm = mon_coor(1:num_mon,2);
y_utm = mon_coor(1:num_mon,3);
z_utm = mon_coor(1:num_mon,4);
coor_mon = [x_utm,y_utm,z_utm];
[numMonitors, dummy] = size(coor_mon); %length(coor_mon);

mkdir([path,'monitor4movie',dirSeparator,'DIS',dirSeparator]);
mkdir([path,'monitor4movie',dirSeparator,'VEL',dirSeparator]);
mkdir([path,'monitor4movie',dirSeparator,'ACC',dirSeparator]);
mkdir([path,'PGMAP',dirSeparator]);
mkdir([path,'MOVIE',dirSeparator]);
if ispec == 1
    mkdir([path,'monitor4movie',dirSeparator,'SD',dirSeparator]);
    mkdir([path,'monitor4movie',dirSeparator,'SA',dirSeparator]);
end



%% PART 1:
%  Pprocessed synthetics and peak ground motion values
%  Write processed time histories and maps of peak ground motion values
%  TRANSFORM FILES MONITORXXXXX.D INTO monitorXXXXX.d

if iconv_mon == 1
    INFO = load([char(path_mon1),'MONITOR.INFO']);
    t0 = 0;
    T = INFO(1,1);                             %final time
    dt_s = INFO(2,1);                          %deltat simulation
    ndt_monit = INFO(3,1);
    dt = ndt_monit*dt_s;                       %deltat monitor
    MPI_num_proc = INFO(4,1);                  %number of mpi proc
    MPI_mnt_id = INFO([5:5+MPI_num_proc-1],1); %id mpi for monitors
    
    
    
    % DISPLACEMENT
    MPI_vec = [1:MPI_num_proc];
    rewrite_mon_format(MPI_vec,MPI_mnt_id,path_mon1,path_monitor)
    
    nomefiler=[char(path),'monitor.d',dirSeparator,'monitor00001.d'];
    a = load(nomefiler);
    numPoints = size(a,1);
    lastPoints = fix(a(numPoints,1));
    numPoints = numPoints+1;
    time = a(:,1);
    dt = time(2)-time(1);
    
else
    nomefiler=[char(path),'monitor.d',dirSeparator,'monitor00001.d'];
    a = load(nomefiler);
    numPoints = size(a,1);
    lastPoints = fix(a(numPoints,1));
    numPoints = numPoints+1;
    time = a(:,1);
    dt = time(2)-time(1);
end

% numPoints = 500;
% lastPoints = fix(a(numPoints,1));
% numPoints = numPoints+1;
% time = a(1:500,1);
% lastPoints = 0.5;

% ---------------------------------------------
% -----------------   STEP   ------------------
vect = [1:numMonitors];
timeResampling = [0:dt_resamp:lastPoints];
numPointsResampling = length(timeResampling);
vect_loc = vect;
alpha = data.ANGLE;

% ***********************************************************
signal = 'dis';
[max_d, max_v, max_a] = ...
    rw_vector_par(signal,path,dirSeparator,...
    vect_loc,numPoints,scale,...
    filtra,n,freq_low,freq_high,freq_highz,...
    alpha,timeResampling,...
    ispec,Tn,nT,zeta);

pgd_loc = max_d;
pgv_loc = max_v;
pga_loc = max_a;

irot = data.IROT;

%writePGMAP_par(path,'dis',dirSeparator,pgd_loc,scale,irot);
%writePGMAP_par(path,'vel',dirSeparator,pgv_loc,scale,irot);
%writePGMAP_par(path,'acc',dirSeparator,pga_loc,scale,irot);

%writePGMAP_4GIS_par(path,'dis',dirSeparator,pgd_loc,scale,x_utm,y_utm);
%writePGMAP_4GIS_par(path,'vel',dirSeparator,pgv_loc,scale,x_utm,y_utm);
%writePGMAP_4GIS_par(path,'acc',dirSeparator,pga_loc,scale,x_utm,y_utm);


%% PART 2: WIRTE MOVIE AND PGV 4 PARAVIEW
if (data.IMOVIE == 1)
    
    if imov(1) == 'd'
        lab = 'Dis';
    elseif imov(1) == 'v'
        lab = 'Vel';
    elseif imov(1) == 'a'
        lab = 'Acc';
    else
        lab = 'NaN';
    end
    
    movie_name = ['Frame_',lab];
    
    numResampling = numPointsResampling;
    write_movie_PARAVIEW(movie_name,imov,path,dirSeparator,...
        vect,numResampling,irot,nlab,lab);
    
end

%% PART 3: Map of response spectral values

if ispec == 1
    rsmap_name_a = ['PSA_Tn'];
    rsmap_name_d = ['SD_Tn'];
    % structural periods for map
    vectPer = [0.1:0.1:1.0,1.25:0.25:5.0];
    writeRSMAP_4GIS(rsmap_name_a,rsmap_name_d,path,...
        dirSeparator,vect,Tn,nT,vectPer,...
        x_utm,y_utm);
end



disp(['TOTAL CPU time ',char(num2str(cputime-tstart)),' sec.']);


