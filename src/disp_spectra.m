%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function ymax = disp_spectra(u,dt,Tn,zeta)

% Compute displacement spectrum for a signal (u), read as an 
% equispaced (dt) column vector 
%
% INPUT:
%   u    = signal 
%   dt   = time step
%   Tn   = period
%   zeta = damping
 

npun=length(u);
if (Tn==0),
    On=1e9;
else
    On=2*pi/Tn;
end;

fac1=1/dt^2;
fac2=zeta*On/dt;

%condizioni iniziali di quiete
ym1=0;
y0=0;
y=ones(1,npun);
y(1)=ym1;
y(2)=y0;

for i=2:npun-1,
   yp1=((2*fac1-On^2)*y0+(-fac1+fac2)*ym1-u(i))/(fac1+fac2);
   y(i+1)=yp1;
   ym1=y0;
   y0=yp1;
end

ymax=max(abs(y));
