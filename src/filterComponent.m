%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function [vx,vy,vz,vp,vo] = filterComponent(filtra,dt,freq_low,freq_high,n,...
                                            vx,vy,vz,vp,vo,ibb,freq_highz)


%------------------------------------------------------------------
% Filtro Butterworth passabanda del III ordine nella banda definita
% da Wn
if filtra == 1
    fnyq=1/2/dt;
%     Nfft=length(vx);
%     df=1/Nfft/dt;
%     freq=[0:df:(Nfft-1)*df];
    Wn = [freq_low freq_high]/fnyq;
    [c,d] = butter(n,Wn);  

    vx = filter(c,d,vx);
    vy = filter(c,d,vy);
    vp = filter(c,d,vp);
    vo = filter(c,d,vo);
    if ibb == 0 % not BB 
        vz = filtfilt(c,d,vz); 
    elseif ibb == 1 % BB 
        Wn = [freq_low freq_highz]/fnyq;
        [c,d] = butter(n,Wn);  
        vz = filter(c,d,vz); 
    end
    
        
        
end
%------------------------------------------------------------------
        



