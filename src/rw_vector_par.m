%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%************************************************************************** 

function [max_dis, max_vel, max_acc] = rw_vector_par(signal,path,dirSeparator,...
                       vect,num_points,scale,...
                       filtra,n,freq_low,freq_high,freq_highz,...
                       alpha,timeResampling,...
                       ispec,Tn,nT,zeta)

 ibb = 0; 
 disp('POST-PROCESSING & WRITING LF TIME HISTORIES');
 % ***********************************************************
  cont = 1; 
    for k = vect
        disp([char(signal),' = ',char(num2str(k))]);
        
        if k<10
            rectxt=['0000',char(num2str(k))];
        elseif k<100
            rectxt=['000',char(num2str(k))];
        elseif k<1000
            rectxt=['00',char(num2str(k))];
        elseif k<10000
            rectxt=['0',char(num2str(k))];
        else
            rectxt=[char(num2str(k))];
        end

        if signal(1:3) == 'dod'
            vector1 = load([char(path),'monitor.d',dirSeparator,'monitor',char(rectxt),'.',char(signal)]);
        else
            vector1 = load([char(path),'monitor.d',dirSeparator,'monitor',char(rectxt),'.',char(signal(1))]);
        end
        vector = vector1(1:num_points-1,:);
        
        dt = vector(2,1)-vector(1,1);
        dt2 = dt*dt;

        %=================================================================%
        %DISPLACEMENT
        %=================================================================%
        sx = [0;vector(1:num_points-1,2)];
        sy = [0;vector(1:num_points-1,3)];
        sz = [0;vector(1:num_points-1,4)];
        t = [0:dt:(num_points-1)*dt]; 
        
        dx = sx;
        dy = sy;
        dz = sz;
        
        [dp,do] = rotationComponent2D(dx,dy,alpha);
        [dx,dy,dz,dp,do] = filterComponent(filtra,dt,freq_low,freq_high,n,...
                                           dx,dy,dz,dp,do,ibb,freq_highz);
        
        [max_dx(cont),max_dy(cont),max_dz(cont),...
         max_dp(cont),max_do(cont),max_dh(cont),...
         max_d(cont),max_dgh(cont)] = maxComponent(dx,dy,dz,dp,do);
         
        
        %=================================================================%
        %VELOCITY
        %=================================================================%
        dsxdt(2:num_points-1) = (dx(3:num_points) - dx(1:num_points-2))./(2*dt);
        dsydt(2:num_points-1) = (dy(3:num_points) - dy(1:num_points-2))./(2*dt);
        dszdt(2:num_points-1) = (dz(3:num_points) - dz(1:num_points-2))./(2*dt);
        
        dsxdt(1) = 0.0; dsxdt(num_points) = dsxdt(num_points-1);
        dsydt(1) = 0.0; dsydt(num_points) = dsydt(num_points-1);
        dszdt(1) = 0.0; dszdt(num_points) = dszdt(num_points-1);
        
        vx = dsxdt;
        vy = dsydt;
        vz = dszdt;
        
        [vp,vo] = rotationComponent2D(vx,vy,alpha);
     
        [max_vx(cont),max_vy(cont),max_vz(cont),...
         max_vp(cont),max_vo(cont), max_vh(cont),...
         max_v(cont),max_vgh(cont)] = maxComponent(vx,vy,vz,vp,vo);

        
        %=================================================================%
        %ACCELERATION
        %=================================================================%
        d2sxdt2(2:num_points-1) = (dx(3:num_points) - 2.*dx(2:num_points-1) + dx(1:num_points-2))./(dt2);
        d2sydt2(2:num_points-1) = (dy(3:num_points) - 2.*dy(2:num_points-1) + dy(1:num_points-2))./(dt2);
        d2szdt2(2:num_points-1) = (dz(3:num_points) - 2.*dz(2:num_points-1) + dz(1:num_points-2))./(dt2);

        d2sxdt2(1) = 0.0; d2sxdt2(num_points) = d2sxdt2(num_points-1);
        d2sydt2(1) = 0.0; d2sydt2(num_points) = d2sydt2(num_points-1);
        d2szdt2(1) = 0.0; d2szdt2(num_points) = d2szdt2(num_points-1);
        
        ax = d2sxdt2;
        ay = d2sydt2;
        az = d2szdt2;

        
        [ap,ao] = rotationComponent2D(ax,ay,alpha);

        [max_ax(cont),max_ay(cont),max_az(cont),...
         max_ap(cont),max_ao(cont),max_ah(cont),...
         max_a(cont),max_agh(cont)] = maxComponent(ax,ay,az,ap,ao);
     
         
        %=================================================================%
        % RESPONSE SPECTRA
        %=================================================================%
        if ispec == 1
        % Compute displacement, (pseudo) velocity and (pseudo) acceleration
        % response spectra 
        for iT = 1:nT
            sdx(iT) = disp_spectra(ax,dt,Tn(iT),zeta);
            psax(iT) = sdx(iT).*(2*pi/Tn(iT)).^2; 
            sdy(iT) = disp_spectra(ay,dt,Tn(iT),zeta);
            psay(iT) = sdy(iT).*(2*pi/Tn(iT)).^2;             
            sdz(iT) = disp_spectra(az,dt,Tn(iT),zeta);
            psaz(iT) = sdz(iT).*(2*pi/Tn(iT)).^2;    
            sdp(iT) = disp_spectra(ap,dt,Tn(iT),zeta);
            psap(iT) = sdp(iT).*(2*pi/Tn(iT)).^2;     
            sdo(iT) = disp_spectra(ao,dt,Tn(iT),zeta);
            psao(iT) = sdo(iT).*(2*pi/Tn(iT)).^2;    
        end
        sdx(1) = 0; psax(1) = max_ax(cont); 
        sdy(1) = 0; psay(1) = max_ay(cont); 
        sdz(1) = 0; psaz(1) = max_az(cont); 
        sdp(1) = 0; psap(1) = max_ap(cont); 
        sdo(1) = 0; psao(1) = max_ao(cont); 
        
        sdgmh = sqrt(sdx.*sdy); 
        psagmh = sqrt(psax.*psay); 
        
       writeRespSpectra(path,rectxt,'sd',dirSeparator,...
           Tn,sdx,sdy,sdz,sdp,sdo,sdgmh,scale);  
       writeRespSpectra(path,rectxt,'sa',dirSeparator,...
           Tn,psax,psay,psaz,psap,psao,psagmh,scale);
        end 
        
      % resampling displ/vel/acc. time histories 
       [dx,dy,dz,dp,do] = resampleComponent(t,dx,dy,dz,dp,do,...
                                             timeResampling);         
                                         
      writeResampledComponent(path,rectxt,'d',dirSeparator,...
                                timeResampling,dx,dy,dz,dp,do,scale);  
      [vx,vy,vz,vp,vo] = resampleComponent(t,vx,vy,vz,vp,vo,...
                                             timeResampling); 
      writeResampledComponent(path,rectxt,'v',dirSeparator,...
                                timeResampling,vx,vy,vz,vp,vo,scale);                       
      [ax,ay,az,ap,ao] = resampleComponent(t,ax,ay,az,ap,ao,...
                                             timeResampling);
      writeResampledComponent(path,rectxt,'a',dirSeparator,...
                                timeResampling,ax,ay,az,ap,ao,scale);
             

     cont = cont +1; 
     
     
    end
nn = cont -1; 
max_dis = zeros(nn,8);
max_vel = zeros(nn,8);
max_acc = zeros(nn,8);

max_dis = [max_dx(:),max_dy(:),max_dz(:),...
              max_dp(:),max_do(:), max_dh(:),...
              max_d(:),max_dgh(:)];
max_vel = [max_vx(:),max_vy(:),max_vz(:),...
              max_vp(:),max_vo(:), max_vh(:),...
              max_v(:),max_vgh(:)];
max_acc = [max_ax(:),max_ay(:),max_az(:),...
              max_ap(:),max_ao(:), max_ah(:),...
              max_a(:),max_agh(:)];   
          
return 



      
   