%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function writeRespSpectra(path,rectxt,signal,dirSeparator,...
                                 Tn,specx,specy,specz,...
                                 specp,speco,specgmh,cost)

%--------------------------------------------------------------------------
num = size(specx,2);
if signal(2) == 'd'
    nomefilew=[char(path),'monitor4movie',dirSeparator,'SD',dirSeparator,'',char(rectxt),'.',char(signal)];
elseif signal(2) == 'a'
    nomefilew=[char(path),'monitor4movie',dirSeparator,'SA',dirSeparator,'',char(rectxt),'.',char(signal)];
end

fid = fopen(nomefilew,'w');
fprintf(fid,'%+9.5f\t %+9.5f\t %+9.5f\t %+9.5f\t %+9.5f\t %+9.5f\t %+9.5f\n',...
            [Tn; [specx.*cost]; [specy.*cost]; ...
            [specz.*cost]; [specp.*cost];...
            [speco.*cost]; [specgmh.*cost]]);

fclose(fid);
%------------------------------------------------------------------
        



