%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function writePGMAP_4GIS_par(path,signal,dirSeparator,...
                             max_vector,cost,x_coor,y_coor)

num = size(max_vector,1); 
max_vectorx = max_vector(:,1)';
max_vectory = max_vector(:,2)';
max_vectorz = max_vector(:,3)';
max_vectorp = max_vector(:,4)';
max_vectoro = max_vector(:,5)';
max_vectorh = max_vector(:,6)';
max_vectord = max_vector(:,7)';
max_vectorgh = max_vector(:,8)';

if signal(1) == 'd'
    type = 'PGD'; 
elseif signal(1) == 'v'
    type = 'PGV';
elseif signal(1) == 'a'
    type = 'PGA'; 
end    

name = [type,'.shp'];
nomefilew=[char(path),'PGMAP',dirSeparator,name];

[PGMAP(1:num).Geometry] = deal('Point');
for ii = 1:num
    [PGMAP(ii).X] = x_coor(ii); 
    [PGMAP(ii).Y] = y_coor(ii);
    if signal(1) == 'd'
        [PGMAP(ii).PGDx] = max_vectorx(ii).*cost;
        [PGMAP(ii).PGDy] = max_vectory(ii).*cost;
        [PGMAP(ii).PGDz] = max_vectorz(ii).*cost;
        [PGMAP(ii).PGDp] = max_vectorp(ii).*cost;
        [PGMAP(ii).PGDo] = max_vectoro(ii).*cost;
        [PGMAP(ii).PGDgmh] = max_vectorgh(ii).*cost;
    elseif signal(1) == 'v'
        [PGMAP(ii).PGVx] = max_vectorx(ii).*cost;
        [PGMAP(ii).PGVy] = max_vectory(ii).*cost;
        [PGMAP(ii).PGVz] = max_vectorz(ii).*cost;
        [PGMAP(ii).PGVp] = max_vectorp(ii).*cost;
        [PGMAP(ii).PGVo] = max_vectoro(ii).*cost;
        [PGMAP(ii).PGVgmh] = max_vectorgh(ii).*cost;        
    elseif signal(1) == 'a'
        [PGMAP(ii).PGAx] = max_vectorx(ii).*cost;
        [PGMAP(ii).PGAy] = max_vectory(ii).*cost;
        [PGMAP(ii).PGAz] = max_vectorz(ii).*cost;
        [PGMAP(ii).PGAp] = max_vectorp(ii).*cost;
        [PGMAP(ii).PGAo] = max_vectoro(ii).*cost;
        [PGMAP(ii).PGAgmh] = max_vectorgh(ii).*cost;
    end
end
shapewrite(PGMAP,nomefilew); 



