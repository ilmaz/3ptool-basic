%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function [max_vx,max_vy,max_vz,...
          max_vp,max_vo,...
          max_vh,max_v,max_vgh] = maxComponent(vx,vy,vz,vp,vo)

                                        

%------------------------------------------------------------------
% Chiara Smerzini, July 2010 
% see Douglas "Earthquake ground motion estimation using strong-motion records:
% a review of equations for the estimation of peak ground
% acceleration and response spectral ordinates"
% Earth-Science Reviews, 61 (2003), pp. 43�104

max_vx = max(abs(vx));    
max_vy = max(abs(vy));      
max_vz = max(abs(vz));
max_vp = max(abs(vp));    
max_vo = max(abs(vo));
% max_vh = max(sqrt(vx.^2 + vy.^2));
% max_v = max(sqrt(vx.^2 + vy.^2 + vz.^2));
max_vh = mean([max_vx,max_vy]); % arithmetic mean (horizontal) 
max_v = mean([max_vx,max_vy,max_vz]);% arithmetic mean (all components) 
%  max_vgh = max(sqrt(abs(vx.*vy)));
max_vgh = sqrt(max_vx.*max_vy); % geometric mean 

%------------------------------------------------------------------
        



