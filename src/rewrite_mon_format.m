%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function rewrite_mon_format(MPI_vec_loc,MPI_mnt_id,path_mon1,...
                           path_monitor)

% Convert monitor format

Num_of_tot_mon = 0;
for q = 1:length(MPI_vec_loc)
     i = MPI_vec_loc(q); 
     
    disp(['Processing MONITOR ',num2str(i),' for Displacement...']);
    
    filename1 = 'MONITORXXXXX.D';
    filename2 = 'MONITORXXXXX.INFO';
    
    if(MPI_mnt_id(i) ~= 0 && i <= 10)
            filename1 = ['MONITOR0000',num2str(i-1),'.D'];
            filename2 = ['MONITOR0000',num2str(i-1),'.INFO'];
        elseif(MPI_mnt_id(i) ~= 0 && i <= 100)
            filename1 = ['MONITOR000',num2str(i-1),'.D'];
            filename2 = ['MONITOR000',num2str(i-1),'.INFO'];
        elseif(MPI_mnt_id(i) ~= 0 && i <= 1000)
            filename1 = ['MONITOR00',num2str(i-1),'.D'];
            filename2 = ['MONITOR00',num2str(i-1),'.INFO'];
        elseif(MPI_mnt_id(i) ~= 0 && i <= 10000)
            filename1 = ['MONITOR0',num2str(i-1),'.D'];
            filename2 = ['MONITOR0',num2str(i-1),'.INFO'];
        elseif(MPI_mnt_id(i) ~= 0 && i <= 100000)
            filename1 = ['MONITOR',num2str(i-1),'.D'];
            filename2 = ['MONITOR',num2str(i-1),'.INFO'];
    end
        
        
        fid = fopen([path_mon1,filename2]);        
        
        if(fid ~= -1)
            
            ST = fclose(fid);
            INFO_MONITOR = load([path_mon1,filename2]);
            Num_of_mon = INFO_MONITOR(1);
            Id_of_mon = INFO_MONITOR([2:2+Num_of_mon-1]);
            Num_of_tot_mon = Num_of_tot_mon + Num_of_mon;
%             
            VAL_MONITOR = load([path_mon1,filename1]);
            k = 0;
            for j = 1: Num_of_mon
                time = VAL_MONITOR(:,1);
                values = VAL_MONITOR(:,[2+k:4+k]);
                
                
                if(Id_of_mon(j) < 10)
                    datafilename = ['monitor0000',num2str(Id_of_mon(j)),'.d'];
                elseif(Id_of_mon(j) < 100)
                    datafilename = ['monitor000',num2str(Id_of_mon(j)),'.d'];
                elseif(Id_of_mon(j) < 1000)
                    datafilename = ['monitor00',num2str(Id_of_mon(j)),'.d'];
                elseif(Id_of_mon(j) < 10000)
                    datafilename = ['monitor0',num2str(Id_of_mon(j)),'.d'];
                elseif(Id_of_mon(j) < 100000)
                    datafilename = ['monitor',num2str(Id_of_mon(j)),'.d'];
                end
                
                file_id = fopen([path_monitor,datafilename], 'w');
                for h = 1: length(time)
                    fprintf(file_id, '%10.8e   %10.8e   %10.8e  %10.8e \n', time(h), values(h,1), values(h,2), values(h,3));
                end
                fclose(file_id);
                
                k = k + 3;
            end

        end
        disp('Done');
        
end
