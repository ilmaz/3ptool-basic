%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function [tri] = vtk_vector_out(fname, x, y, z, v, tri, name)


fid = fopen(fname, 'w');
%%intestazione del file, richiesta dal formato
fprintf(fid, '# vtk DataFile Version 2.0\n');
fprintf(fid, 'Comment\n');
%%tipo file: in questo caso ascii
fprintf(fid, 'ASCII\n');
%%tipo di dati: ci sono anche structured grid e altre
fprintf(fid, 'DATASET UNSTRUCTURED_GRID\n');
%%punti della mesh
fprintf(fid,'POINTS %i float\n', length(x));
for i=1:length(x)
    fprintf(fid,'%g %g %g \n', x(i), y(i), z(i));
end
%if nargin < 5
%    tri = delaunay(x,y,z)-1; %%l'indice per vtk parte da 0
%end
numtria = length(tri(:,1));
%%stampo la connettività di griglia
fprintf(fid,'CELLS %i %i\n',numtria, 4*numtria);
for i=1:numtria
    fprintf(fid,'3 %i %i %i\n', tri(i,1)-1, tri(i,2)-1, tri(i,3)-1);
end
%%tipo di celle: immaigno 5 indichi il triangolo, ma è da verificare
fprintf(fid,'CELL_TYPES %i\n', numtria);
for i=1:numtria
    fprintf(fid,'5\n' );
end
%%dati puntuali => uno per nodo della griglia
fprintf(fid,'POINT_DATA %i\n', length(x));

%%scalari
fprintf(fid,['SCALARS  ', name,'_x float 1\n']);
%%lookup table non so bene cosa sia, ma ci vuole
fprintf(fid,'LOOKUP_TABLE default\n');
for i=1:length(x)
    fprintf(fid,'%g\n', v(i,1));
end
%%idem
fprintf(fid,['SCALARS  ', name,'_y float 1\n']);
fprintf(fid,'LOOKUP_TABLE default\n');
for i=1:length(x)
    fprintf(fid,'%g\n', v(i,2));
end
%%idem
fprintf(fid,['SCALARS  ', name,'_z float 1\n']);
fprintf(fid,'LOOKUP_TABLE default\n');
for i=1:length(x)
    fprintf(fid,'%g\n', v(i,3));
end
%%vettore
fprintf(fid,['VECTORS  ' ,name, '  float\n']);
for i=1:length(x)
    fprintf(fid,'%g %g %g\n',v(i,1), v(i,2), v(i,3));
end
fclose(fid);
