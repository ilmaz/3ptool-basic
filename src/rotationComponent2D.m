%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function [vp,vo] = rotationComponent2D(vx,vy,alpha)
% alpha = azimuth measured clockwise from the North (in rad) 
% vx = east-west component 
% vy = north-south component
if alpha ~= 0 
    vp = vx.*sin(alpha) + vy.*cos(alpha); % -> parallel  
    vo = vx.*cos(alpha) - vy.*sin(alpha); % -> orthogonal   
else
    vp = vx;
    vo = vy;
end
        



