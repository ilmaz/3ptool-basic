%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%************************************************************************** 

function write_movie_PARAVIEW(mov_name,imov,path,dirSeparator,...
    vect,num,irot,nlab,lab)

if imov(1) == 'd'
    j = 0;
    ux = zeros(length(vect),num);
    uy = zeros(length(vect),num);
    uz = zeros(length(vect),num);
    up = zeros(length(vect),num);
    uo = zeros(length(vect),num);
    disp('DISPLACEMENT')
    for k = vect
        disp(['Displ = ',char(num2str(k))])
        j = j+1;
        if k<10
            rectxt=['0000',char(num2str(k))];
        elseif k<100
            rectxt=['000',char(num2str(k))];
        elseif k<1000
            rectxt=['00',char(num2str(k))];
        elseif k<10000
            rectxt=['0',char(num2str(k))];
        else
            rectxt=[char(num2str(k))];
        end
        
        nomefiler=[char(path),'monitor4movie',dirSeparator,'DIS',dirSeparator,char(rectxt),'.d'];
        a = load(nomefiler);
        num_points = length(a);
        t = a(1:num_points,1);
        dt = t(2)-t(1);
        ux(j,1:num_points) = a(1:num_points,2);
        uy(j,1:num_points) = a(1:num_points,3);
        uz(j,1:num_points) = a(1:num_points,4);
        up(j,1:num_points) = a(1:num_points,5);
        uo(j,1:num_points) = a(1:num_points,6);
    end
elseif imov(1) == 'v'
    j = 0;
    vx = zeros(length(vect),num);
    vy = zeros(length(vect),num);
    vz = zeros(length(vect),num);
    vp = zeros(length(vect),num);
    vo = zeros(length(vect),num);
    disp('VELOCITY')
    for k = vect
        disp(['Vel = ',char(num2str(k))])
        j = j+1;
        if k<10
            rectxt=['0000',char(num2str(k))];
        elseif k<100
            rectxt=['000',char(num2str(k))];
        elseif k<1000
            rectxt=['00',char(num2str(k))];
        elseif k<10000
            rectxt=['0',char(num2str(k))];
        else
            rectxt=[char(num2str(k))];
        end
        
        nomefiler=[char(path),'monitor4movie',dirSeparator,'VEL',dirSeparator,char(rectxt),'.v'];
        a = load(nomefiler);
        num_points = length(a);
        t = a(:,1);
        dt = t(2)-t(1);
        vx(j,1:num_points) = a(:,2);
        vy(j,1:num_points) = a(:,3);
        vz(j,1:num_points) = a(:,4);
        vp(j,1:num_points) = a(:,5);
        vo(j,1:num_points) = a(:,6);
    end
elseif imov(1) == 'a'
    j = 0;
    ax = zeros(length(vect),num);
    ay = zeros(length(vect),num);
    az = zeros(length(vect),num);
    ap = zeros(length(vect),num);
    ao = zeros(length(vect),num);
    disp('ACCELERATION')
    for k = vect
        disp(['Acc = ',char(num2str(k))])
        j = j+1;
        if k<10
            rectxt=['0000',char(num2str(k))];
        elseif k<100
            rectxt=['000',char(num2str(k))];
        elseif k<1000
            rectxt=['00',char(num2str(k))];
        elseif k<10000
            rectxt=['0',char(num2str(k))];
        else
            rectxt=[char(num2str(k))];
        end
        
        nomefiler=[char(path),'monitor4movie',dirSeparator,'ACC',dirSeparator,char(rectxt),'.a'];
        a = load(nomefiler);
        num_points = length(a);
        t = a(:,1);
        dt = t(2)-t(1);
        ax(j,1:num_points) = a(:,2);
        ay(j,1:num_points) = a(:,3);
        az(j,1:num_points) = a(:,4);
        ap(j,1:num_points) = a(:,5);
        ao(j,1:num_points) = a(:,6);
    end
end

% ***********************************************************
% **                  LOAD GEOMETRY                        **
% ***********************************************************


nomefiler=[char(path),'monitors.txt'];

fid = fopen(nomefiler,'r');
tab=7;
ctria=0;
grid=0;
num_ctria=0;

for i= 1:8
    tline = fgetl(fid);
end

num_nodes = str2num(tline(20-tab:length(tline)-1));
tline = fgetl(fid);
num_elem = str2num(tline(19-tab:length(tline)-1));
tline = fgetl(fid);
num_el_blk = str2num(tline(21-tab:length(tline)-1));
tline = fgetl(fid);
tline = fgetl(fid);

for i=1:num_el_blk
    num_el_in_blk(i)=str2num(tline(25-tab+fix(i/10):length(tline)-1));
    tline = fgetl(fid);
    num_nod_per_el(i)=str2num(tline(26-tab+fix(i/10):length(tline)-1));
    tline = fgetl(fid);
    if num_nod_per_el(i)==3
        num_ctria = num_ctria +  num_el_in_blk(i);
    end
    tline = fgetl(fid);
end

num_ctria;

l_tline=8;
tline = fgetl(fid);
i=0;

con_ctria=zeros(num_ctria,3);

grid_cord=zeros(num_nodes*3,1);

ctria_tag=zeros(num_ctria,1);
ctria_id=zeros(num_ctria,1);

grid_cord=zeros(num_nodes,3);
grid_id=zeros(num_nodes,1);
grid_x=zeros(num_nodes,1);
grid_y=zeros(num_nodes,1);
grid_z=zeros(num_nodes,1);

ctria_1=zeros(num_ctria,1);
ctria_2=zeros(num_ctria,1);
ctria_3=zeros(num_ctria,1);



for i=1:num_el_blk
    
    if i==1
        tline = fgetl(fid);
        disp(['Reading ',char(num2str(i)),'st block  (',char(num2str(num_el_in_blk(i))),' elem.)'])
    elseif i==2
        disp(['completed in ',char(num2str(cputime-tini)),' sec.'])
        disp(['Reading ',char(num2str(i)),'nd block  (',char(num2str(num_el_in_blk(i))),' elem.)'])
    elseif i==3
        disp(['completed in ',char(num2str(cputime-tini)),' sec.'])
        disp(['Reading ',char(num2str(i)),'rd block  (',char(num2str(num_el_in_blk(i))),' elem.)'])
    else
        disp(['completed in ',char(num2str(cputime-tini)),' sec.'])
        disp(['Reading ',char(num2str(i)),'th block  (',char(num2str(num_el_in_blk(i))),' elem.)'])
    end
    
    tini=cputime;
    
    while strcmp(tline(1:l_tline),' connect')~=1
        tline = fgetl(fid);
        if length(tline)<8
            l_tline=length(tline);
        elseif length(tline)==0
            l_tline=1;
        else
            l_tline=8;
        end
    end
    
    if num_nod_per_el(i)==3
        for j=1:num_el_in_blk(i)
            tline = fgetl(fid);
            %pos=findstr(tline,',');
            ctria=ctria+1;
            ctria_id(ctria)=ctria;
            ctria_tag(ctria)=i;
            con_ctria(ctria,:)=str2num(tline);
        end
    end
end

disp(['completed in ',char(num2str(cputime-tini)),' sec.'])

tini=cputime;
disp(['*** Storing data informations ***'])

if ctria>0
    ctria_1(1:ctria)=con_ctria(1:ctria,1);
    ctria_2(1:ctria)=con_ctria(1:ctria,2);
    ctria_3(1:ctria)=con_ctria(1:ctria,3);
end

disp(['completed in ',char(num2str(cputime-tini)),' sec.'])

tini=cputime;
disp(['BEGIN - Reading nodes coordinates'])

%clear pos;

tline = fgetl(fid);
tline = fgetl(fid);
vero=[];
grid=1;

while strcmp(tline(1:l_tline),' coord =')~=1
    %for i= 1:23+2*num_el_blk
    tline = fgetl(fid);
    if length(tline)<8
        l_tline=length(tline);
    elseif length(tline)==0
        l_tline=1;
    else
        l_tline=8;
    end
end

while length(vero)==0
    %clear pos;
    tline = fgetl(fid);
    vero=findstr(tline,';');
    
    if length(vero)==0
        
        howmanynumb=length(str2num(tline));
        grid_cord(grid:grid+howmanynumb-1)=str2num(tline);
        grid=grid+howmanynumb;
        
    else
        howmanynumb=length(str2num(tline));
        grid_cord(grid:grid+howmanynumb-1)=str2num(tline);
        grid=grid+howmanynumb;
    end
end

grid=num_nodes;
for i=1:num_nodes
    grid_id(i)=i;
end
grid_x(1:num_nodes)=grid_cord(1:num_nodes);
grid_y(1:num_nodes)=grid_cord(num_nodes+1:2*num_nodes);
grid_z(1:num_nodes)=grid_cord(2*num_nodes+1:3*num_nodes);

fclose(fid);


disp(['END - Reading nodes coordinates in ',char(num2str(cputime-tini)),' sec.'])




filenamew = [path,dirSeparator,'MOVIE',dirSeparator,'PARA.coord'];

fid = fopen(filenamew,'w');

tini=cputime;
disp(['BEGIN - Writing input format for PARAVIEW']);

if grid>0
    fprintf(fid,'%+13.7e  %+13.7e  %+13.7e\n',[grid_x(1:grid)'; grid_y(1:grid)'; grid_z(1:grid)']);
end

fclose(fid);
coord = load(filenamew);
disp(['End Coordinates']);


disp(['Elements']);

filenamew = [path,dirSeparator,'MOVIE',dirSeparator,'PARA.tria'];
fid = fopen(filenamew,'w');

if ctria>0
    fprintf(fid,'%i    %i    %i  \n',...
        [ctria_1(1:ctria)'; ctria_2(1:ctria)'; ctria_3(1:ctria)']);
end

fclose(fid);
tri = load(filenamew);

disp(['End Elements']);



x = coord(:,1);
y = coord(:,2);
z = coord(:,3);





% ***********************************************************
% **           WRITING THE VTK                  **
% ***********************************************************

nlab = 1;
%matlabpool('open','local',nlab);
%kappa_vec = distributed([1:num]);


%spmd
%kappa_loc = getLocalPart(kappa_vec);

for k = 1: 1: num
    
    nomefilew = [char(path),'MOVIE',dirSeparator,char(mov_name),'_',num2str(k),'.vtk']
    fid = fopen(nomefilew,'w');
    
    
    if imov(1) == 'd'
        
        if irot == 1
            val = [up(:,k) uo(:,k) uz(:,k)];
            [tri] = vtk_vector_out(nomefilew, x, y, z, val , tri, lab);
        else
            val = [ux(:,k) uy(:,k) uz(:,k)];
            [tri] = vtk_vector_out(nomefilew, x, y, z, val, tri, lab);
        end
        
    elseif imov(1) == 'v'
        
        if irot == 1
            val = [vp(:,k) vo(:,k) vz(:,k)];
            [tri] = vtk_vector_out(nomefilew, x, y, z, val, tri, lab);
        else
            val = [vx(:,k) vy(:,k) vz(:,k)];
            [tri] = vtk_vector_out(nomefilew, x, y, z, val, tri, lab);
        end
        
    elseif imov(1) == 'a'
        
        if irot == 1
            val = [ap(:,k) ao(:,k) az(:,k)];
            [tri] = vtk_vector_out(nomefilew, x, y, z, val, tri, lab);
        else
            val = [ax(:,k) ay(:,k) az(:,k)];
            [tri] = vtk_vector_out(nomefilew, x, y, z, val, tri, lab);
        end
    end
    fclose(fid);
end
% end


% ***********************************************************
% **           WRITING THE PGMAPS                  **
% ***********************************************************
disp(['WRITING PGMAP for PARAVIEW'])


if imov(1) == 'd'
    nomefilew = [char(path),'MOVIE',dirSeparator,'PGD.vtk'];
    fid = fopen(nomefilew,'w');
    
    max_val_x = max(ux,[],2);
    max_val_y = max(uy,[],2);
    max_val_z = max(uz,[],2);
    values = [max_val_x, max_val_y, max_val_z];
    
    val = [max(values,[],2), max(values,[],2), max(values,[],2)];
    [tri] = vtk_vector_out(nomefilew, x, y, z, val, tri, 'PGD');
    
    fclose(fid);
end
if imov(1) == 'v'
    
    nomefilew = [char(path),'MOVIE',dirSeparator,'PGV.vtk'];
    fid = fopen(nomefilew,'w');
    
    max_val_x = max(vx,[],2);
    max_val_y = max(vy,[],2);
    max_val_z = max(vz,[],2);
    values = [max_val_x, max_val_y, max_val_z];
    
    val = [max(values,[],2), max(values,[],2), max(values,[],2)];
    [tri] = vtk_vector_out(nomefilew, x, y, z, val, tri, 'PGV');
    
    fclose(fid);
       
    
end
if imov(1) == 'a'
    nomefilew = [char(path),'MOVIE',dirSeparator,'PGA.vtk'];
    fid = fopen(nomefilew,'w');
    
    max_val_x = max(ax,[],2);
    max_val_y = max(ay,[],2);
    max_val_z = max(az,[],2);
    values = [max_val_x, max_val_y, max_val_z];
    
    val = [max(values,[],2), max(values,[],2), max(values,[],2)];
    [tri] = vtk_vector_out(nomefilew, x, y, z, val, tri, 'PGA');
    
    fclose(fid);
end


%matlabpool close


