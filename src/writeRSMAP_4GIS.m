%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function writeRSMAP_4GIS(rsm_name_a,rsm_name_d,path,...
                         dirSeparator,vect,per,num,...
                         vectPer,x_coor,y_coor)

tol = 1e-10; 
%  sd 
j = 0;
sdx = zeros(length(vect),num);
sdy = zeros(length(vect),num);
sdz = zeros(length(vect),num);
sdp = zeros(length(vect),num);
sdo = zeros(length(vect),num);
sdgmh = zeros(length(vect),num);
disp('DISPLACEMENT RESPONSE SPECTRA')
for k = vect
    disp(['SD = ',char(num2str(k))])
    j = j+1;
    if k<10
        rectxt=['0000',char(num2str(k))];
    elseif k<100
        rectxt=['000',char(num2str(k))];
    elseif k<1000
        rectxt=['00',char(num2str(k))];
    elseif k<10000
        rectxt=['0',char(num2str(k))];
    else
        rectxt=[char(num2str(k))];
    end
    
    nomefiler=[char(path),'monitor4movie',dirSeparator,'SD',dirSeparator,char(rectxt),'.sd'];
    a = load(nomefiler);
    num_points = length(a);
    Tn = a(:,1);
    dTn = Tn(2)-Tn(1);
    sdx(j,1:num_points) = a(:,2);
    sdy(j,1:num_points) = a(:,3);
    sdz(j,1:num_points) = a(:,4);
    sdp(j,1:num_points) = a(:,5);
    sdo(j,1:num_points) = a(:,6); 
    sdgmh(j,1:num_points) = a(:,7);
end

% sa == 1
j = 0;
psax = zeros(length(vect),num);
psay = zeros(length(vect),num);
psaz = zeros(length(vect),num);
psap = zeros(length(vect),num);
psao = zeros(length(vect),num);
psagmh = zeros(length(vect),num);
disp('PSEUDO-ACC RESPONSE SPECTRA')
for k = vect
    disp(['PSA = ',char(num2str(k))])
    j = j+1;
    if k<10
        rectxt=['0000',char(num2str(k))];
    elseif k<100
        rectxt=['000',char(num2str(k))];
    elseif k<1000
        rectxt=['00',char(num2str(k))];
    elseif k<10000
        rectxt=['0',char(num2str(k))];
    else
        rectxt=[char(num2str(k))];
    end
    
    nomefiler=[char(path),'monitor4movie',dirSeparator,'SA',dirSeparator,char(rectxt),'.sa'];
    a = load(nomefiler);
    num_points = length(a);
    Tn = a(:,1);
    dTn = Tn(2)-Tn(1);
    psax(j,1:num_points) = a(:,2);
    psay(j,1:num_points) = a(:,3);
    psaz(j,1:num_points) = a(:,4);
    psap(j,1:num_points) = a(:,5);
    psao(j,1:num_points) = a(:,6); 
    psagmh(j,1:num_points) = a(:,7);
end

% ***********************************************************
% **      WRITING RESPONSE SPECTRAL MAP  for ArcGIS        **
% ***********************************************************
for j = 1:size(vectPer,2) 
%         in_per = find(Tn==vectPer(j)); 
        in_per = find((per>=vectPer(j)-tol)&(per<=vectPer(j)+tol)); 
        period = vectPer(j); 
        
        nomefilew_a=[char(path),'PGMAP',dirSeparator,num2str(rsm_name_a),num2str(period),'.shp'];
        nomefilew_d=[char(path),'PGMAP',dirSeparator,num2str(rsm_name_d),num2str(period),'.shp'];
        
        
        [PSA_MAP(1:length(vect)).Geometry] = deal('Point');
        [SD_MAP(1:length(vect)).Geometry] = deal('Point');
        for ii = 1:length(vect)
            [PSA_MAP(ii).X] = x_coor(ii); 
            [PSA_MAP(ii).Y] = y_coor(ii);
            [PSA_MAP(ii).PSAx] = psax(ii,in_per);
            [PSA_MAP(ii).PSAy] = psay(ii,in_per);
            [PSA_MAP(ii).PSAz] = psaz(ii,in_per);
            [PSA_MAP(ii).PSAp] = psap(ii,in_per);
            [PSA_MAP(ii).PSAo] = psao(ii,in_per);
            [PSA_MAP(ii).PSAgmh] = psagmh(ii,in_per);
            
            [SD_MAP(ii).X] = x_coor(ii); 
            [SD_MAP(ii).Y] = y_coor(ii);
            [SD_MAP(ii).SDx] = sdx(ii,in_per);
            [SD_MAP(ii).SDy] = sdy(ii,in_per);
            [SD_MAP(ii).SDz] = sdz(ii,in_per);
            [SD_MAP(ii).SDp] = sdp(ii,in_per);
            [SD_MAP(ii).SDo] = sdo(ii,in_per);
            [SD_MAP(ii).SDgmh] = sdgmh(ii,in_per);
        end
        shapewrite(PSA_MAP,nomefilew_a); 
        shapewrite(SD_MAP,nomefilew_d); 
        
end

