%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************

function writePGMAP_par(path,signal,dirSeparator,...
                                 max_vector,cost,irot)
%------------------------------------------------------------------
num =[1:size(max_vector,1)];
max_vectorx = max_vector(:,1)';
max_vectory = max_vector(:,2)';
max_vectorz = max_vector(:,3)';
max_vectorp = max_vector(:,4)';
max_vectoro = max_vector(:,5)';
max_vectorh = max_vector(:,6)';
max_vectord = max_vector(:,7)';
max_vectorgh = max_vector(:,8)';

if signal(1) == 'd';
    type = 'PGD'; 
elseif signal(1) == 'v';
    type = 'PGV';
else signal(1) == 'a';
    type = 'PGA'; 
end
    
name = [type,'.res']; 
nomefilew = [char(path),'PGMAP',dirSeparator,name];
if irot == 1 
    label = 'poz'; 
    vect_out = [num; max_vectorp.*cost; max_vectoro.*cost; max_vectorz.*cost];
else
    label = 'xyz'; 
    vect_out = [num; max_vectorx.*cost; max_vectory.*cost; max_vectorz.*cost];
end

fid = fopen(nomefilew,'w');
if labindex == 1 
if signal(1) == 'd'
    fprintf(fid,['       DIS_',num2str(label),'       1    +0.0        3  1    0\n']);
elseif signal(1) == 'v'
    fprintf(fid,['       VEL_',num2str(label),'       1    +0.0        3  1    0\n']);
elseif signal(1) == 'a'
    fprintf(fid,['       ACC_',num2str(label),'       1    +0.0        3  1    0\n']);
end
end
fprintf(fid,'%i %+4.2f\t %+4.2f\t %+4.2f\n',vect_out);
fclose(fid);
   
%------------------------------------------------------------------
       