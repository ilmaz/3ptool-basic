%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano 
%         P.zza Leonardo da Vinci, 32 
%         20133 Milano 
%         Italy                                        
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%************************************************************************** 


function MAIN(varargin)

% MATLAB code for POST-PROCESSING the SPEED results 
% Set up the following variables according to your data  

addpath src

%% DIRECTORY WHERE MONITORS ARE STORED
% in the same folder put
% LS.input with the list of monitored points 
% monitors.txt defining the triangulated surface to be exported for paraview
% if you want to make a movie
% (vertex of triangles are the points listed in LS.input)
handles.DIR = '/home/ilario/Scrivania/WORKinPROGRESS/SOFI/plane_wave/';
% SELECT THE OPERATING SISTEM: 'Unix' or 'Dos '
handles.OS = 'Unix';

%% OPTIONS

%REWRITE MONITORS FROM MONITORXXXXXX.Z TO monitorxxxxxx.z (1/0 = YES/NO)
handles.ICONV_MON = 1;

%NUMBER OF MONITORS TO BE PROCESSED (SET 0 FOR PROCESSING ALL MONITORS)
handles.NUM_OF_MON = 0;

%% FILTERING

%SCALING FACTOR (1 = m, 100 = cm)
handles.SCALE = 1;

%FILTER (1/0 = YES/NO) 
handles.FILTER = 0;

%LOW PASS FREQUENCY
handles.LP_FREQ = 0.05;

%HIGH PASS FREQUENCY
handles.HP_FREQ = 1;

%RESAMPLING FOR DT 
handles.RESAMPLED_DT = 0.01;% 0.01;

%ROTATED COMPONENT (w.r.t. y-axis clock wise)
handles.IROT = 0; %1 rotation, 0 no rotation
handles.ANGLE = 0;

%% OUTPUT 

%MOVIE (1/0 = YES/NOT)
handles.IMOVIE = 1;

%DISPLACEMENT
handles.DIS = 1;

%VELOCITY
handles.VEL = 0;

%ACCELERATION
handles.ACC = 0;

%COMPUTE RESPONSE SPECTRA (1/0 = YES/NOT)
handles.RES_SPEC = 0;

%% POST-PROCESSING

postprocessing(handles);

close all;
disp('END');

