%% PLOT THE RESULTS
clear
% close all
clc

path1 = '/home/ilario/Scrivania/WORKinPROGRESS/SOFI/plane_wave/monitor.d/';
path2 = '/home/ilario/Scrivania/WORKinPROGRESS/SOFI/plane_wave/monitor_mf_p4.d/';

t0 = 0;
T = 5;


for i = 100
    if i < 10
        fileName = ['monitor0000',num2str(i),'.d'];
    elseif i < 100
        fileName = ['monitor000',num2str(i),'.d'];
    elseif i < 1000
        fileName = ['monitor00',num2str(i),'.d'];
    elseif i < 10000
        fileName = ['monitor0',num2str(i),'.d'];
    elseif i < 100000
        fileName = ['monitor',num2str(i),'.d'];
    end
    
    sol_1 = load([path1,fileName]);
    sol_2 = load([path2,fileName]);
    
    
    
    
    
    figure(i)
%     subplot(311)
    
    hold on
    plot(sol_1(:,1),sol_1(:,2),'r-','LineWidth',2);hold on; grid on;
    plot(sol_2(:,1),sol_2(:,2),'k-','LineWidth',1);hold on; grid on;
    
    xlim([t0 T]);
%     ylim([-0.001 0.001]);
    title(['Monitor ', num2str(i)]);
    xlabel('t (s)');
    ylabel('u_x (m)');
    
%     subplot(312)
%     plot(sol_1(:,1),sol_1(:,3),'r-','LineWidth',2);hold on; grid on;
%     plot(sol_2(:,1),sol_2(:,3),'k-','LineWidth',1);hold on; grid on;
%     ylim([-0.01 0.01]);
%     
%     xlim([t0 T]);
%     xlabel('t (s)');
%     ylabel('u_y (m)');
%     
%     subplot(313)
%     hold on
%     plot(sol_1(:,1),sol_1(:,4),'r-','LineWidth',2);hold on; grid on;
%     plot(sol_2(:,1),sol_2(:,4),'k-','LineWidth',1);hold on; grid on;
%     xlim([t0 T]);
%     ylim([-0.01 0.01])
    legend('POLIMI','MUNICH-RE')
    xlabel('t (s)');
    ylabel('u_z (m)');
    
%     pause;
end



